const onClick = (index) => {
    const activeIndex = [...document.querySelectorAll('.water-item')].findIndex(v => v.className.indexOf('active') >= 0)
    const currColorsItem = dcMain.querySelector(`#m${index} .water-ld`)
    const activeColorsItem = dcMain.querySelector(`#m${activeIndex} .water-ld`)
}

    if (activeColorsItem && index !== activeIndex) {
        const activeColorLength = activeColorsItem.children.length
        const currColorLength = currColorsItem.children.length

        if (activeColorLength === 0) {
            dcMain.querySelector(`#m${activeIndex}`).classList.remove('active')
            return alert('空瓶子无法再倒水了')
        }
        if (currColorLength && activeColorLength) {
            if (activeColorsItem.children[activeColorLength - 1].style.backgroundColor !== currColorsItem.children[currColorLength - 1].style.backgroundColor) {
                return alert('只有相同颜色才可以倒入')
            }
            if (currColorsItem.children.length >= allData[allDataIndex].colorCount) {
                return alert('该瓶子水位已满')
            }
        }
    }
    const activeEl = dcMain.querySelector(`#m${activeIndex}`)
    const currEl = dcMain.querySelector(`#m${index}`)
    if (activeIndex === index) {
        activeEl.classList.remove('active')
    } else if (activeIndex === -1) {
        currEl.classList.add('active')
    } else {
        let r1 = currEl.getBoundingClientRect()
        let r2 = activeEl.getBoundingClientRect()
        let x = 0
        let y = 0
        let rotate = ''}
        if (r1.left < r2.left) {
            rotate = '-90'
            x = r1.height - (r1.top - r2.top - 30) + 'px'
            y = (r1.left - r2.left + r1.height / 2 - 2) + 'px'
        } else {
            rotate = '90'
            x = -(r1.height - (r1.top - r2.top - 30)) + 'px'
            y = (r2.left - r1.left + r1.height / 2 - 3) + 'px'
        }
        // 举杯移动
        activeEl.setAttribute('style', `transform: rotate(${rotate}deg) translate(${x}, ${y});`)
        activeEl.querySelector('.water-ld').setAttribute('style', `transform: rotate(${-rotate}deg) scale(5, 0.25)`)
        const activeColors = [...activeColorsItem.children].map(v => v.style.backgroundColor)

        // 检测有没有并行色，计算数量
        const forCount = activeColors.reverse().reduce((sum, v, i, arr) => {
            if (arr[i - 1] && v === arr[i - 1]) {
                return sum + 1
            }
            return sum
        }, 1)

        // active水量减少，curr水量增多
        const colorElAll = activeEl.querySelectorAll('.wi-color')
        for (let i = 0; i < forCount; i++) {
            const colorIndex = activeColors.length - (1 + i)
            colorElAll[colorIndex].style.height = 0
            currEl.querySelector('.water-ld').appendChild(colorElAll[colorIndex].cloneNode())
            colorElAll[colorIndex].ontransitionend = () => {
                colorElAll[colorIndex].parentNode.removeChild(colorElAll[colorIndex])
            }

        }