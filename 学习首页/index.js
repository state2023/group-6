// 轮播图
var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    paginationClickable: true,
    spaceBetween: 30,
    centeredSlides: true,
    autoplay: 2500,
    autoplayDisableOnInteraction: false
  });
  // 滚动文字
  marquee("marquee", "marquee_text");
  function marquee(p, s) {
    var scrollWidth = document.getElementById(p).offsetWidth;
    var textWidth = document.getElementById(s).offsetWidth;
    var i = scrollWidth;
  
    function change() {
      i--;
      if (i < -textWidth) {
        i = scrollWidth;
      }
      document.getElementById(s).style.left = i + "px";
      window.requestAnimationFrame(change);
    }
    window.requestAnimationFrame(change);
  }
  