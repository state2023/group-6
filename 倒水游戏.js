const allData = [{
  maxCount: 3,
  colorCount: 2,
  emptyCount: 1
}, {
  maxCount: 5,
  colorCount: 3,
  emptyCount: 2
}, {
  maxCount: 7,
  colorCount: 4,
  emptyCount: 3
}, {
  maxCount: 8,
  colorCount: 5,
  emptyCount: 3
}, {
  maxCount: 9,
  colorCount: 6,
  emptyCount: 3
}, {
  maxCount: 10,
  colorCount: 7,
  emptyCount: 3
}, {
  maxCount: 12,
  colorCount: 8,
  emptyCount: 4
}]
const colorList = ['#8f1b1b', '#905019', '#ffffff', '#542943', '#366aff', '#5d581b', '#ad37b5', '#f96767', '#143a22']
let allDataIndex = 0
const dcText = document.querySelector('.dc-text')
const dcMain = document.querySelector('.dc-main')
const onNext = (index) => {
  if (index < 0 || index >= allData.length) {
    return
  }
  dcText.innerHTML = `${index + 1}/${allData.length}`
  allDataIndex = index
  renderHtml(allData[allDataIndex])
}

const renderHtml = (rule) => {
  dcMain.innerHTML = `<div class="bar" style="display:none"></div>
      ${Array.from(new Array(rule.maxCount)).map((v, i) => {
    const haveColorIndex = rule.maxCount - rule.emptyCount
    const arr = colorList.slice(0, rule.colorCount).sort(v => Math.random() - 0.5)
    let colors = []
    if (i < haveColorIndex) {
      colors = arr.map(v => ({ height: 100 / rule.colorCount + '%', color: v }))
    }

    return `<div class="water-ls">
                  <div class="water-item" onclick="onClick(${i})" id="m${i}">
                      <div class="water-ld">
                          ${colors.map(v => {
      return `<div class="wi-color" style="background-color:${v.color};height:${v.height}"></div>`
    }).join('')}
                            </div>
                  </div>
              </div>`
  }).join('')
    }`
}